0.2.0
-----
2019/02/10

- added preservation feature which allows to specify chunks of data not to be
  flattened
- added support of boolean data



0.1.0 (first release)
---------------------
2019/02/08

- allows to flatten data in JSONB fields of postgres tables easily from a
  description of the data structure
